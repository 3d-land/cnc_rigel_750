﻿# CNC Rigel Model 750



Modeling in FreeCAD 0.19


## Images
![CNC_Rigel 750 Rnder](/Images/CNC_Rigel_750.png)

## License 


[GNU AFFERO GENERAL PUBLIC LICENSE](https://www.gnu.org/licenses/agpl-3.0.en.html)

## Attributions

See commit details to find the authors of each Part.
- @fandres7_7
